﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGYM.Models
{
    public class ActivityModel
    {

        public int ID { set; get; }
        [Required]
        public String Title { set; get; }
        [Required]
        public String Descript { set; get; }
        [Required]
        [Range(1, 30, ErrorMessage = "La capacidad minimo es 1 y máxima 30")]
        public int  Assistance{ set; get; }
        [Required]
        [MinLength(1)]
        [MaxLength(2)]
        public int Duration { set; get; }

        public Boolean Active { set; get; }

        [InverseProperty("ActivityModel")]
        public List<ClassModel> Classes { set; get; }

        [ForeignKey("TrainerID")]
        public Trainer Trainer { set; get; }

        [Display(Name = "Trainer")]
        public int TrainerID { set; get; }

    }
}
