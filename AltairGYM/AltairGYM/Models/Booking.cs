﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AltairGYM.Models
{
    public class Booking
    {
        public int ID { get; set; }
        [Required]
        [DataType(DataType.Time)]
        public  DateTime Date { get; set; }

        [ForeignKey("CustomerID")]
        public Customer Customer { get; set; }
        [Display(Name ="Customer")]
        public int CustomerID { get; set; }

        [ForeignKey("ClassModelID")]
        public ClassModel Class { get; set; }

        [Display(Name =" ClassModel")]
        public int ClassID { get; set; }
    }
}