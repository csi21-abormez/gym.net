﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGYM.Models
{
    public class ClassModel
    {
        public int ID { get; set; }
        [Required]
        [DataType(DataType.Time)]
        public DateTime DateClass { get; set; }
        public DateTime Hour { get; set; }
        [InverseProperty("ClassModel")]
        public List<Booking> Bookings { get; set; }
        [ForeignKey("ActivityID")]
        public ActivityModel Activity { get; set; }

        [Display(Name = "Activity")]
        public int ActivityID { get; set; }

        [ForeignKey("RoomID")]
        public Room Room { get; set; }

        [Display(Name = "RoomID")]
        public int RoomID { get; set; }
    }
}
