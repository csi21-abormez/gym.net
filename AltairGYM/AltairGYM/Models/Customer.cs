﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGYM.Models
{
    public class Customer : Person
    {
        public int Weigth { set; get; }

        public int Heigth { set; get; }

        [InverseProperty("BookingID")]
        public List<Booking> Bookings {set;get;}

    }

   
}
