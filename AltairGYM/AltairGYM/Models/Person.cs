﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGYM.Models
{
    public abstract class Person
    {

        public int ID { set; get; }
        [Required] 
        public String Name { set; get; }
        [Required]
        public String Surname { set; get; }
        [Required]
        [EmailAddress]
        public String Email { set; get; }
        [Phone]
        [Required]
        [RegularExpression (@"^[0-9]{9}$")]
        public String Phone { set; get; }
        [Required]
        
        public int Age{ set; get; }
        [Required]
        public String Nick { set; get; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        [Required]
        public String Rol { set; get; }
    }
}
