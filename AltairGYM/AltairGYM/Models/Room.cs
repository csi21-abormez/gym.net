﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGYM.Models
{
    public class Room
    {
        public String Name { get; set; }
        public int Capacity { get; set; }

        [ForeignKey("ClassModelID")]
        public ClassModel Class { get; set; }
        [Display(Name = "ClassModel")]
        public int ClassID { get; set; }
    }
}
