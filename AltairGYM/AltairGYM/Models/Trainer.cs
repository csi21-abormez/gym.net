﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGYM.Models
{
    public class Trainer:Person
    {
        [InverseProperty("Trainer")]
        public List<ActivityModel> Activities { get; set; }



    }
}
