﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AltairGym.Data;
using AltairGym.Models;

namespace AltairGym.Controllers
{
    public class ClassModelsController : Controller
    {
        private readonly AltairGymContext _context;

        public ClassModelsController(AltairGymContext context)
        {
            _context = context;
        }

        // GET: ClassModels
        public async Task<IActionResult> Index()
        {
            var altairGymContext = _context.ClassModel.Include(c => c.ActivityModel).Include(c => c.Room);
            return View(await altairGymContext.ToListAsync());
        }

        // GET: ClassModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classModel = await _context.ClassModel
                .Include(c => c.ActivityModel)
                .Include(c => c.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (classModel == null)
            {
                return NotFound();
            }

            return View(classModel);
        }

        // GET: ClassModels/Create
        public IActionResult Create()
        {
            ViewData["ActivityModelID"] = new SelectList(_context.ActivityModel, "ID", "Descript");
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "ID");
            return View();
        }

        // POST: ClassModels/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,DateClass,Hour,ActivityModelID,RoomID")] ClassModel classModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(classModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ActivityModelID"] = new SelectList(_context.ActivityModel, "ID", "Descript", classModel.ActivityModelID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "ID", classModel.RoomID);
            return View(classModel);
        }

        // GET: ClassModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classModel = await _context.ClassModel.FindAsync(id);
            if (classModel == null)
            {
                return NotFound();
            }
            ViewData["ActivityModelID"] = new SelectList(_context.ActivityModel, "ID", "Descript", classModel.ActivityModelID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "ID", classModel.RoomID);
            return View(classModel);
        }

        // POST: ClassModels/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,DateClass,Hour,ActivityModelID,RoomID")] ClassModel classModel)
        {
            if (id != classModel.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(classModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClassModelExists(classModel.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ActivityModelID"] = new SelectList(_context.ActivityModel, "ID", "Descript", classModel.ActivityModelID);
            ViewData["RoomID"] = new SelectList(_context.Room, "ID", "ID", classModel.RoomID);
            return View(classModel);
        }

        // GET: ClassModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classModel = await _context.ClassModel
                .Include(c => c.ActivityModel)
                .Include(c => c.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (classModel == null)
            {
                return NotFound();
            }

            return View(classModel);
        }

        // POST: ClassModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var classModel = await _context.ClassModel.FindAsync(id);
            _context.ClassModel.Remove(classModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClassModelExists(int id)
        {
            return _context.ClassModel.Any(e => e.ID == id);
        }
    }
}
