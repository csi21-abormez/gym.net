﻿using System;
using AltairGym.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace AltairGym.Data
{
    public partial class AltairGymContext : DbContext
    {
        public AltairGymContext()
        {
        }

        public AltairGymContext(DbContextOptions<AltairGymContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("server=DESKTOP-K6URJIS;database=AltairGym;Integrated Security=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            OnModelCreatingPartial(modelBuilder);
            modelBuilder.Entity<ActivityModel>().ToTable("Activity");
            modelBuilder.Entity<Booking>().ToTable("Booking");
            modelBuilder.Entity<ClassModel>().ToTable("ClassModel");
            modelBuilder.Entity<Customer>().ToTable("Customer");
            modelBuilder.Entity<Room>().ToTable("Room");
            modelBuilder.Entity<Trainer>().ToTable("Trainer");

        }
    

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    

        public DbSet<AltairGym.Models.ActivityModel> ActivityModel { get; set; }
    

        public DbSet<AltairGym.Models.Booking> Booking { get; set; }
    

        public DbSet<AltairGym.Models.ClassModel> ClassModel { get; set; }
    

        public DbSet<AltairGym.Models.Customer> Customer { get; set; }
    

        public DbSet<AltairGym.Models.Room> Room { get; set; }
    

        public DbSet<AltairGym.Models.Trainer> Trainer { get; set; }

    }
}
