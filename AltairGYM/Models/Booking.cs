﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Booking
    {
        public int ID { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Display(Name = "Customer")]
        public int CustomerID { get; set; }

        [ForeignKey("CustomerID")]
        public Customer Customer { get; set; }
        

        [Display(Name = " ClassModel")]
        public int ClassModelID { get; set; }

        [ForeignKey("ClassModelID")]
        public ClassModel ClassModel { get; set; }

        

    }
}
