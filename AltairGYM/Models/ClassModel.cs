﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class ClassModel
    {
        public int ID { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateClass { get; set; }

        [DataType(DataType.Time)]
        public DateTime Hour { get; set; }

        [InverseProperty("ClassModel")]
        public List<Booking> Bookings { get; set; }

        [Display(Name = "ActivityModel")]
        public int ActivityModelID { get; set; }

        [ForeignKey("ActivityModelID")]
        public ActivityModel ActivityModel { get; set; }

        [Display(Name = "RoomID")]
        public int RoomID { get; set; }

        [ForeignKey("RoomID")]
        public Room Room { get; set; }

        
        

    }
}
