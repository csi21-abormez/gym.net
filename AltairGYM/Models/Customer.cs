﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Customer:Person
    {
        public int Weigth { set; get; }

        public int Heigth { set; get; }

        [InverseProperty("Customer")]
        public List<Booking> Bookings { set; get; }

    }
}
