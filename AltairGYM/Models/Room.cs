﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Room
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public int Capacity { get; set; }

        [InverseProperty("Room")]
        public List<ClassModel> ClassModes { get; set; }
        
      

    }
}
